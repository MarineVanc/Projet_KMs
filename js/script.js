$(function() {
  //index.html
  $('#sign').click(function() {
    $('#sign').addClass('active')
    $('#log').removeClass('active')
    $('#login').hide();
    $('#signup').show();
  })
  $('#log').click(function() {
    $('#log').addClass('active')
    $('#sign').removeClass('active')
    $('#signup').hide();
    $('#login').show();
  })

  /*--------------------------------------------------------------------------*/
  //Profil.html
  $('#password').hide();

  $('#change_password').click(function() {
    $('#password').show();
  });

  /*--------------------------------------------------------------------------*/
  //kilofees.html
  $("input").keyup(function() {
    var travels = $('#travels').val();
    var type = 1
    var km = $('#km').val();
    var cv = $('#cv').val();
    var dtotal = ""

    $("#travels").keyup(function() {
      var travels = $('#travels').val();
      if (isNaN(travels) || travels < 0) {
        $('#travels').css('background', '#bf0e0e')
        return false
      } else {
        $('#travels').css('background', 'white')
      }
    })

    $("#cv").keyup(function() {
      var cv = $('#cv').val();
      if (isNaN(cv) || cv <= 0) {
        $('#cv').css('background', '#bf0e0e')
      } else {
        $('#cv').css('background', 'white')
      }
    })

    $("#km").keyup(function() {
      var km = $('#km').val();
      if (isNaN(km) || km <= 0 || km > 5000) {
        $('#km').css('background', '#bf0e0e')
      } else {
        $('#km').css('background', 'white')
      }
    })

    /*------------------------------------------------------------------------*/
    var coeff_cars = {
      3: 0.41,
      4: 0.493,
      5: 0.543,
      6: 0.568,
      7: 0.595,
    }
    var coeff_motorcycles = {
      1: 0.338,
      3: 0.4,
      6: 0.518
    }
    var coeff_mopeds = {
      1: 0.269,
    }

    /*------------------------------------------------------------------------*/
    if (cv > 0 && cv <= 3) {
      total = ($('#travels').val() * ($('#km').val() * coeff_cars[3]))

    } else if (cv == 4) {
      total = ($('#travels').val() * ($('#km').val() * coeff_cars[$('#cv').val()]))

    } else if (cv == 5) {
      total = ($('#travels').val() * ($('#km').val() * coeff_cars[$('#cv').val()]))

    } else if (cv == 6) {
      total = ($('#travels').val() * ($('#km').val() * coeff_cars[$('#cv').val()]))

    } else if (cv >= 7) {
      total = ($('#travels').val() * ($('#km').val() * coeff_cars[7]))

    }

    $('#total').val(total.toFixed(2) + ' €')


  })
  return false
})
